package com.huntington.callum.productive;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoteViewFragment extends Fragment {


    public NoteViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //retrieve fragment layout so that we can use it to get the refs of the widgets contained in it
        View fragmentLayout = inflater.inflate(R.layout.fragment_note_view, container, false);

        //get all the widgets needed so that we can fill them with appropriate note data
        TextView title = (TextView) fragmentLayout.findViewById(R.id.viewNoteTitle);
        TextView message = (TextView) fragmentLayout.findViewById(R.id.viewNoteMessage);
        ImageView icon = (ImageView) fragmentLayout.findViewById(R.id.viewNoteIcon);

        //get the intent sent to NoteDetailActivity from the MainActivityListFragment, which contains note data extras
        Intent intent = getActivity().getIntent();

        //get the title and message of the note from intent extras
        title.setText(intent.getExtras().getString(MainActivity.NOTE_TITLE_EXTRA));
        message.setText(intent.getExtras().getString(MainActivity.NOTE_MESSAGE_EXTRA));

        //get the category from the intent and use the categoryToDrawable method retrieve appropriate image and set it to imageView
        Note.Category noteCat = (Note.Category) intent.getSerializableExtra(MainActivity.NOTE_CATEGORY_EXTRA);
        icon.setImageResource(Note.categoryToDrawable(noteCat));

        //layout all modified now
        return fragmentLayout;
    }
}
