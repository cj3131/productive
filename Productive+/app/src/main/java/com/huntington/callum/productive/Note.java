package com.huntington.callum.productive;

/**
 * Created by Callum Huntington on 28/11/2016.
 */

public class Note {

    private String title, message;
    private long noteId, dateCreatedMilli;
    private Category category;

    public enum Category{ BLACKPAGE, NOTEBOOK, TRANSPARENTTICK, WHITEPAGE }

    public Note(String title, String message, Category category){
        this.title = title;
        this.message = message;
        this.category = category;
        this.noteId = 0;
        this.dateCreatedMilli = 0;
    }

    public Note(String title, String message, Category category, long noteId, long dateCreatedMilli) {
        this.title = title;
        this.message = message;
        this.category = category;
        this.noteId = noteId;
        this.dateCreatedMilli = dateCreatedMilli;
    }

    public String getTitle(){
        return title;
    }

    public String getMessage(){
        return message;
    }

    public Category getCategory() {
        return category;
    }

    //never actually got this implemented
    public long getDate(){ return dateCreatedMilli;}

    public long getId(){return noteId;}

    public String toString() {
        return "ID: " + noteId + " Title: " + title + " Message: " + message + " IconID: " + category.name() + " Date: ";
    }

    public int getAssociatedDrawable(){
        return categoryToDrawable(category);
    }

    public static int categoryToDrawable(Category noteCategory){

        switch(noteCategory){
            case BLACKPAGE:
                return R.drawable.blackpage;
            case NOTEBOOK:
                return R.drawable.notebook;
            case WHITEPAGE:
                return R.drawable.transparenttick;
            case TRANSPARENTTICK:
                return R.drawable.whitepage;
        }
        return R.drawable.blackpage;
    }
}
